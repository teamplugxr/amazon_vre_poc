var list_items = document.querySelectorAll('#workStation>li');
var body_items = document.querySelectorAll('#bodystation>li');
var analysis_items = document.querySelectorAll('#analysisStation>li');
var risk_items = document.querySelectorAll('#riskOptions>li');
var riskSelect = document.getElementById("riskSelect");
var dataSelect = document.getElementById("dataSelect");



for (var i = 0; i < list_items.length; i++) {
    list_items[i].addEventListener("click", toggle);
    list_items[i].addEventListener("tap", toggle);
}
for (var i = 0; i < body_items.length; i++) {
    body_items[i].addEventListener("click", bodyToggle);
}
for (var i = 0; i < analysis_items.length; i++) {
    analysis_items[i].addEventListener("click", analysisToggle);
}
for (var i = 0; i < risk_items.length; i++) {
    risk_items[i].addEventListener("click", riskToggle);
}
// riskSelect.addEventListener('click',reportsToggle2);
dataSelect.addEventListener('click',reportsToggle1);


function toggle() {
    $("#workSelect").removeClass("current");
    $("#workSelect").removeClass("active");
    $("#tnsSelect").removeClass("current");
    $("#tnsSelect").removeClass("active");
    $("#shipSelect").removeClass("current");
    $("#shipSelect").removeClass("active");
    $("#afeSelect").removeClass("current");
    $("#afeSelect").removeClass("active");
    this.classList.add("current");
    this.classList.add("active");
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $("#btnIndex").css("background", "green");
        $('#infoForButton').hide();
    }
}

function bodyToggle() {
    $("#rsSelect").removeClass("current");
    $("#rsSelect").removeClass("active");
    $("#lsSelect").removeClass("current");
    $("#lsSelect").removeClass("active");
    $("#rwSelect").removeClass("current");
    $("#rwSelect").removeClass("active");
    $("#lwSelect").removeClass("current");
    $("#lwSelect").removeClass("active");
    $("#lbSelect").removeClass("current");
    $("#lbSelect").removeClass("active");
    this.classList.add("current");
    this.classList.add("active");
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $("#btnIndex").css("background", "green");
        $('#infoForButton').hide();
    }
}

function analysisToggle() {
    $("#postureSelect").removeClass("current");
    $("#postureSelect").removeClass("active");
    $("#durationSelect").removeClass("current");
    $("#durationSelect").removeClass("active");
    $("#frequencySelect").removeClass("current");
    $("#frequencySelect").removeClass("active");
    $("#fatigueSelect").removeClass("current");
    $("#fatigueSelect").removeClass("active");
    this.classList.add("current");
    this.classList.add("active");
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $("#btnIndex").css("background", "green");
        $('#infoForButton').hide();
    }
}

function reportsToggle1() {
    $("#dataSelect").removeClass("current");
    $("#dataSelect").removeClass("active");
    $("#riskSelect").removeClass("current");
    $("#riskSelect").removeClass("active");
    $("#r1Select").removeClass("current");
    $("#r1Select").removeClass("active");
    $("#r2Select").removeClass("current");
    $("#r2Select").removeClass("active");
    $("#r3Select").removeClass("current");
    $("#r3Select").removeClass("active");
    console.log("the report this:",this);
    this.classList.add("current");
    this.classList.add("active");
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $("#btnIndex").css("background", "green");
        $('#infoForButton').hide();
    }
    $("#r1Select").removeClass("current");
    $("#r1Select").removeClass("active");
    $("#r2Select").removeClass("current");
    $("#r2Select").removeClass("active");
    $("#r3Select").removeClass("current");
    $("#r3Select").removeClass("active");
}
// $("#workStation").click(function() {
//     // $("#workSelect").toggleClass("current");
//     this.classList.toggle("current");
// });

function reportsToggle2() {
    $("#dataSelect").removeClass("current");
    $("#dataSelect").removeClass("active");
    $("#riskSelect").removeClass("current");
    $("#riskSelect").removeClass("active");
    $("#r1Select").removeClass("current");
    $("#r1Select").removeClass("active");
    $("#r2Select").removeClass("current");
    $("#r2Select").removeClass("active");
    $("#r3Select").removeClass("current");
    $("#r3Select").removeClass("active");
    console.log("the report this:",this);
    riskSelect.classList.add("current");
    riskSelect.classList.add("active");
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $("#btnIndex").css("background", "green");
        $('#infoForButton').hide();
    }
    $("#r1Select").find('.bullet').css("background-color", "white");
    $("#r2Select").find('.bullet').css("background-color", "white");
    $("#r3Select").find('.bullet').css("background-color", "white");
}

function riskToggle(){
    $("#dataSelect").removeClass("current");
    $("#dataSelect").removeClass("active");
    $("#riskSelect").removeClass("current");
    $("#riskSelect").removeClass("active");
    $("#r1Select").removeClass("current");
    $("#r1Select").removeClass("active");
    $("#r2Select").removeClass("current");
    $("#r2Select").removeClass("active");
    $("#r3Select").removeClass("current");
    $("#r3Select").removeClass("active");
    // $("#r1Select").find('.bullet').css("background-color", "white");
    // $("#r2Select").find('.bullet').css("background-color", "white");
    // $("#r3Select").find('.bullet').css("background-color", "white");
    // if(this.id=="r1Select"){
    //     $("#r1Select").find('.bullet').css("background-color", "");
    // }
    // if(this.id=="r2Select"){
    //     $("#r2Select").find('.bullet').css("background-color", "");
    // }
    // if(this.id=="r3Select"){
    //     $("#r3Select").find('.bullet').css("background-color", "");
    // }
    
    this.classList.add("current");
    this.classList.add("active");
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $("#btnIndex").css("background", "green");
        $('#infoForButton').hide();
    }
}

$('#btnIndex').click(function() {
    if ($("#r1Select").hasClass('active') && $("#postureSelect").hasClass('active') && $("#rsSelect").hasClass('active')) {
        $('#btnIndex').prop('disabled', true);
        $("#btnIndex").css("background", "green");
        location.href = 'analysis2.html';
    } else {
        $('#btnIndex').prop('disabled', false);
        $('#infoForButton').show();
    }


});

var dots = window.setInterval( function() {
    var wait = document.getElementsByClassName("wait");
    for(var i=0;i<wait.length;i++){
        if ( wait[i].innerHTML.length > 2 ) 
            wait[i].innerHTML = "";
        else 
            wait[i].innerHTML += ".";
    }
    
    }, 200);

    // window.dotsGoingUp = true;
    // var dots = window.setInterval( function() {
    //     var wait = document.getElementsByClassName("wait");
    // for(var i=0;i<wait.length;i++){
    //     if ( window.dotsGoingUp ) 
    //         wait[i].innerHTML += ".";
    //     else {
    //         wait[i].innerHTML = wait[i].innerHTML.substring(1, wait[i].innerHTML.length);
    //         if ( wait[i].innerHTML === "")
    //             window.dotsGoingUp = true;
    //     }
    //     if ( wait[i].innerHTML.length > 1 )
    //         window.dotsGoingUp = false;
    // }
    //     }, 100);

document.getElementById('liveVid').addEventListener('ended',myHandler,false);
// myHandler();
    function myHandler(e) {
        var riskText = document.getElementById("riskText");
        var riskTextc = document.getElementById("riskTextc");
        // var shoulderText = document.getElementById("riskText");
        var riskText1 = document.getElementById("riskText1");
        var riskText2 = document.getElementById("riskText2");
        var riskImage1 = document.getElementById("riskImage1");
        var riskImage2 = document.getElementById("riskImage2");
        var riskImage3 = document.getElementById("riskImage3");
        var middleSection = document.getElementById("middleSection");
        var middleSection1 = document.getElementById("middleSection1");


        // riskText.innerHTML="<p> Risk Assessment completed</p>";
        riskImage1.style.display="block";
        riskImage2.style.display="block";
        riskImage3.style.display="block";
        riskTextc.innerHTML="Risk Assessment Completed"
        riskText.innerHTML="<p> Low Risk</p>";
        riskText1.innerHTML="<p> Medium Risk</p>";
        riskText2.innerHTML="<p> High Risk</p>";
        middleSection.style.display  = "none";
        middleSection1.style.display="block";
        
    }