var treeobj = "ThreedViewer/tree.glb";
var angrybird = "elephant.glb";
var amzStore="./models/Amazon_animation_05.fbx";
var amzBone="Bone_Animation.fbx";
var cottage = "./models/cottage.glb";

var cottageTextures=[
    "models/cottage_diffuse.png",
    "models/cottage_normal.png"
]
var obj,pivot,originalObj,boxhelper,currObj,originalmatarr;
// var width =$("#canvas-element").width();
// var height =400;
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 35,window.innerWidth / window.innerHeight, 0.01, 10000 );
// camera.position.z=0.5;
camera.position.x=0.4;
// camera.position.set(-0.4585250021958123, 1.177186171898454,  0.4277219528403883);

var light = new THREE.DirectionalLight("#0xffffff", 0.5);
var ambient = new THREE.AmbientLight("#0x404040");
light.position.set(10, 15, 10);
light.castShadow = true;
scene.add(light);
scene.add(ambient);

var renderer = new THREE.WebGLRenderer();
renderer.setClearColor("lightgrey", 1);
renderer.setSize(window.innerWidth , window.innerHeight );
renderer.shadowMap.enabled = true;


var controls = new THREE.OrbitControls( camera, renderer.domElement );
controls.update();
var transformControls = new THREE.TransformControls(camera, renderer.domElement);
scene.add(transformControls);

var groundGeo = new THREE.PlaneGeometry( 2.4, 2.4 );
groundGeo.rotateX( - Math.PI / 2 );
var groundMat = new THREE.MeshPhongMaterial({color:"green"});
groundMat.opacity=1;
var ground = new THREE.Mesh( groundGeo, groundMat );
ground.receiveShadow = true;
//scene.add( ground );

let check=document.getElementById("check");
			check.addEventListener("click",function(){
				console.log("cam details: ",camera.position);
				console.log("cam rot: ",camera.rotation);
                console.log("obj pos :",obj.position );

			});
// document.getElementById("available_lifts-img1").append( renderer.domElement );
// document.getElementById("available_lifts-img2").append( renderer.domElement );
// document.getElementById("available_lifts-img3").append( renderer.domElement );
// document.getElementById("available_lifts-img4").append( renderer.domElement );
// document.getElementById("available_lifts-img5").append( renderer.domElement );
// document.getElementById("available_lifts-img6").append( renderer.domElement );

var loader = new THREE.FBXLoader();
loader.load(
	// resource URL
	amzStore,
	// called when the resource is loaded
	function ( gltf ) {
		// gltf.scene.position.y=-0.5;
		obj=gltf;
		
        mixer = new THREE.AnimationMixer( gltf );

        const action = mixer.clipAction( gltf.animations[ 0 ] );
        console.log("animation: ",action);
        action.play();
		


		//to set the pivot point
		var objectBounding = new THREE.BoxHelper(obj);
        var bb = objectBounding.geometry.boundingSphere;
        var objHeight = bb.radius;
        var new_sx = 1 / (4 * objHeight);

        var boundingBox = new THREE.Box3().setFromObject(obj);
    	var size = boundingBox.getSize();
    	console.log("size: ", size);
        objectBounding.geometry.computeBoundingBox();
        var minlen=objectBounding.geometry.boundingBox.min.y;
        var minlenx=objectBounding.geometry.boundingBox.max.x-objectBounding.geometry.boundingBox.max.x;

        console.log("minlenx,y: ",minlenx/2);
        pivot = new THREE.Object3D();
        var group = new THREE.Object3D();
        var objLength = [];

        obj.traverse(function(child) {
            if (child instanceof THREE.Mesh) {
            	console.log("child",child);
                child.castShadow = true;
                child.material.needsUpdate = true;
                child.castShadow = true;
                new THREE.WireframeGeometry( child.geometry );
            }
        });

        for (var c = 0; c < obj.children.length; c++) {
            objLength.push(obj.children[c]);
          
        }
        for (var c = 0; c < objLength.length; c++) {
            group.add(objLength[c]);
        }
        new THREE.Box3().setFromObject( group ).getCenter(group.position ).multiplyScalar(-1);
        console.log((minlen));
        ground.position.y=minlen;
        pivot.add(group);
        pivot.scale.set(new_sx,new_sx,new_sx);
        console.log("pivot",pivot);
        scene.add( pivot );
        console.log("ground",ground);
        originalObj=pivot;
        originalmatarr=[];
        pivot.traverse(function(object) {
		        if ( object.isMesh ) {
		            // originalmatarr.push(object.material.clone());
		        }
		    });
    //   transformControls.attach(pivot);

	},
	// called while loading is progressing
	function ( xhr ) {
		console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
		var percentageLoaded=( xhr.loaded / xhr.total * 100 );
		$("#loading").css("background-color","grey");
		$("#loading").css("height","5px");
		console.log("percentageLoaded",percentageLoaded,percentageLoaded/2);
		$("#loading").css("width",percentageLoaded+"%");
		if(percentageLoaded==100){
			setTimeout(function(){
				console.log("hide");
      	$("#loading").hide();
      },1000);
		}

	},
	// called when loading has errors
	function ( error ) {

		console.log( 'An error happened' ,error);

	}
);

            

function animate() {
	requestAnimationFrame( animate );
	controls.update();
	renderer.render( scene, camera );
}
animate();